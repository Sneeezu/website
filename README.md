# Website

> Neovim configuration tutorial for beginners in czech.

![showcase](docs/showcase.png)

## Building

Clone the repo to your **$GOPATH/src**:

```sh
git clone https://codeberg.org/Sneeezu/website
```

Change directory into it:

```sh
cd website
```

Build it using make

```
make build
```

## Usage

You can use h flag of the binary for usage:

```sh
server -h
```

```
Usage of server:
  -a string
        Set address where to host the site. (default "localhost:3000")
```

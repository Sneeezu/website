module codeberg.org/Sneeezu/website

go 1.22.1

require (
	github.com/a-h/templ v0.2.697
	github.com/alecthomas/chroma/v2 v2.13.0
	github.com/labstack/echo/v4 v4.11.4
	golang.org/x/text v0.14.0
	golang.org/x/time v0.5.0
)

require (
	github.com/dlclark/regexp2 v1.11.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)

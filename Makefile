generate:
	@templ generate

run: generate
	@go run cmd/server/*.go

build:
	@go build -o tmp/server cmd/server/*.go

watch:
	@air --build.cmd "make generate build" --build.bin "tmp/server" --build.include_ext "go,templ,css" --build.exclude_regex "templ.go"

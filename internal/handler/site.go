// Package handler serves the html.
package handler

import (
	"context"
	"embed"
	"net/http"

	"codeberg.org/Sneeezu/website/internal/book"
	"codeberg.org/Sneeezu/website/internal/ptr"

	"github.com/a-h/templ"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/time/rate"
)

//go:embed assets
var currentFs embed.FS

func render(ctx echo.Context, status int, t templ.Component) error {
	ctx.Response().Writer.WriteHeader(status)

	err := t.Render(context.Background(), ctx.Response().Writer)
	if err != nil {
		return ctx.String(http.StatusInternalServerError, "failed to render response template")
	}

	return nil
}

// Site handler.
type Site struct {
	echo echo.Echo
	book book.Book
}

// Serve the website.
func (s *Site) Serve(address string) {
	if s.book.IsEmpty() {
		return
	}

	for node := s.book.Front(); node != nil; node = node.Next() {
		element := node.Value()

		var next *string
		if n := node.Next(); n != nil {
			next = ptr.To(n.Value().Name())
		}

		var prev *string
		if p := node.Prev(); p != nil {
			prev = ptr.To(p.Value().Name())
		}

		template := element.Template(next, prev)

		s.echo.GET("/static/templates/"+element.Name(), func(c echo.Context) error {
			return render(c, 200, template)
		})

		s.echo.GET("/"+element.Name(), func(c echo.Context) error {
			return render(c, 200, book.Main(&s.book, template))
		})
	}

	s.echo.GET("/", func(c echo.Context) error {
		return c.Redirect(http.StatusMovedPermanently, "/"+s.book.Front().Value().Name())
	})

	s.echo.StaticFS("/static", echo.MustSubFS(currentFs, "assets"))

	s.echo.Logger.Fatal(s.echo.Start(address))
}

// New creates new instance of Site.
func New() (s *Site) {
	s = &Site{
		*echo.New(),
		*book.New(),
	}

	s.echo.HideBanner = true
	s.echo.Pre(middleware.RemoveTrailingSlash())
	s.echo.Use(middleware.Logger())
	s.echo.Use(middleware.Recover())
	s.echo.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(
		rate.Limit(20),
	)))

	return
}

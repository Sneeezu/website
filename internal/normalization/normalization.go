// Package normalization normalizes strings
package normalization

import (
	"strings"
	"unicode"

	"golang.org/x/text/unicode/norm"
)

// Normalize string to not contain special characters or spaces
func Normalize(s string) string {
	s = norm.NFD.String(s)

	var result strings.Builder
	for _, r := range s {
		r = unicode.ToLower(r)

		if r == ' ' {
			result.WriteRune('-')
			continue
		}

		if unicode.Is(unicode.Mn, r) {
			continue
		}

		result.WriteRune(r)
	}

	return result.String()
}

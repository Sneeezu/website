// Package list provides doubly linked list because the std one does not have type checking.
package list

// Node is a element of the doubly linked list.
type Node[T any] struct {
	next, prev *Node[T]
	value      T
}

// Next returns the next node or nil.
func (n *Node[T]) Next() *Node[T] {
	return n.next
}

// Prev returns the previous node or nil.
func (n *Node[T]) Prev() *Node[T] {
	return n.prev
}

// Value returns value of the node.
func (n *Node[T]) Value() T {
	return n.value
}

// NewNode return new unlinked node.
func NewNode[T any](value T) *Node[T] {
	return &Node[T]{
		nil,
		nil,
		value,
	}
}

// List is doubly linked list.
type List[T any] struct {
	head, tail *Node[T]
	lenght     int
}

// Front returns head of the doubly linked list or nil.
func (dll *List[T]) Front() *Node[T] {
	return dll.head
}

// Back returns tail of the doubly linked list or nil.
func (dll *List[T]) Back() *Node[T] {
	return dll.tail
}

// IsEmpty returns if the list is empty or not.
func (dll *List[T]) IsEmpty() bool {
	return dll.head == nil
}

// PushBack pushes new value to the end of the doubly linked list.
func (dll *List[T]) PushBack(value T) {
	newNode := &Node[T]{
		nil,
		nil,
		value,
	}

	if dll.head == nil {
		dll.head = newNode
		dll.tail = newNode
		return
	}

	dll.tail.next = newNode
	newNode.prev = dll.tail
	dll.tail = newNode
}

// New returns new instance of doubly linked list.
func New[T any]() *List[T] {
	return &List[T]{
		nil,
		nil,
		0,
	}
}

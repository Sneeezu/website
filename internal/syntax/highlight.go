// Package syntax provides functions for syntax highlighting of code.
package syntax

import (
	"bytes"
	"io"

	"github.com/alecthomas/chroma/v2"
	"github.com/alecthomas/chroma/v2/formatters/html"
	"github.com/alecthomas/chroma/v2/lexers"
	"github.com/alecthomas/chroma/v2/styles"
)

func highlight(w io.Writer, code, lexer, style string) error {
	// Determine lexer.
	l := lexers.Get(lexer)
	if l == nil {
		l = lexers.Analyse(code)
	}

	if l == nil {
		l = lexers.Fallback
	}
	l = chroma.Coalesce(l)

	// Determine style.
	s := styles.Get(style)
	if s == nil {
		s = styles.Fallback
	}

	it, err := l.Tokenise(nil, code)
	if err != nil {
		return err
	}

	f := html.New(html.Standalone(false), html.WithClasses(false))

	return f.Format(w, s, it)
}

// HighlightCode highlights the code passed in or it returns it without it.
func HighlightCode(code, lexer, style string) string {
	var b bytes.Buffer

	err := highlight(&b, code, lexer, style)
	if err != nil {
		return code
	}

	return b.String()
}

package book

templ git(next, prev *string) {
	@content(next, prev) {
		@gitIntro()
		@gitFugitive()
		@gitSigns()
	}
}

templ gitIntro() {
	@h1("Git")
	<p>
		Neovim v základu nemá žádnou git integraci.
		Pro nakonfigurování gitu použijeme dva pluginy.
		<br/>
		<br/>
		Ty jsou:
	</p>
}

templ gitFugitive() {
	@h2("Fugitive")
	<p>
		Fugitve je plugin od legendárního autora Vim pluginů Tpopa.
		Tento plugin umožnuje interagovat s gitem vně Neovimu.
		Rovněž tento plugin umí rešit git <a href="http://vimcasts.org/episodes/fugitive-vim-resolving-merge-conflicts-with-vimdiff/">merge konflikty</a> nebo zobrazovat git log přímo v Neovimu.
		Oproti většině pluginů v tomto tutoriálu, tento plugin je napsán v jazyce Vimscript.
	</p>
	<img src="/static/img/fugitive.png" alt="fugitive"/>
	<p>
		Pro nakonfigurování tohoto pluginu je nutné přidat tento kód do vaší plugin konfigurace:
	</p>
	@code(`return {
	"tpope/vim-fugitive",
	event = "VeryLazy",

	keys = {
		{ "<leader>gs", "<cmd>tab Git<CR>", desc = "Open git status" },
	},
}
`,
		"lua")
}

templ gitSigns() {
	@h2("Git Signs")
	<p>
		<a href="https://github.com/lewis6991/gitsigns.nvim">Git signs</a> je plugin pro zobrazování git změn.
		Plugin mimo to umožnuje skákat k další git změně nebo resetovat git hunky.
	</p>
	<img src="/static/img/git-signs.png" alt="git-signs"/>
	<p>
		Pro nastavení tohoto pluginu je potřeba přidat tento kód do vaší konfigurace:
	</p>
	@code(`return {
	"lewis6991/gitsigns.nvim",
	event = { "BufReadPost", "BufNewFile" },

	cmd = "Gitsigns",

	opts = {
		signs = {
			add = { text = "+" },
			untracked = { text = "+" },
			delete = { text = "▁" },
			topdelete = { text = "▶" },
			change = { text = "~" },
			changedelete = { text = "~" },
		},

		current_line_blame = false,
		current_line_blame_opts = {
			virt_text_pos = "right_align",
			delay = 2000,
		},

		preview_config = {
			border = "rounded",
		},

		attach_to_untracked = true,
		on_attach = function(buffer)
			local function map(mode, l, r, desc)
				vim.keymap.set(mode, l, r, { buffer = buffer, desc = desc })
			end

			map("n", "]c", "<cmd>Gitsigns next_hunk<CR>", "Next git hunk")
			map("n", "[c", "<cmd>Gitsigns prev_hunk<CR>", "Previous git hunk")
			map("n", "<leader>gp", "<cmd>Gitsigns preview_hunk<CR>", "Preview hunk")
			map("n", "<leader>grh", "<cmd>Gitsigns reset_hunk<CR>", "Reset hunk")
			map("n", "<leader>grb", "<cmd>Gitsigns reset_buffer<CR>", "Reset buffer")
			map({ "x", "o" }, "ah", ":<C-U>Gitsigns select_hunk<CR>", "hunk")
			map({ "x", "o" }, "ih", ":<C-U>Gitsigns select_hunk<CR>", "hunk")
		end,
	},
}
`,
		"lua")
}

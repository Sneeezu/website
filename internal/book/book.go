// Package book provides storage for pages.
package book

import (
	"codeberg.org/Sneeezu/website/internal/list"
	"codeberg.org/Sneeezu/website/internal/normalization"

	"github.com/a-h/templ"
)

// Page is element of the storage.
type Page struct {
	name, displayName string
	template          func(next, prev *string) templ.Component
}

// Name returns name of the element.
func (e *Page) Name() string {
	return e.name
}

// DisplayName returns display name of the element.
func (e *Page) DisplayName() string {
	return e.displayName
}

// Template returns template of the element.
func (e *Page) Template(next, prev *string) templ.Component {
	return e.template(next, prev)
}

// NewPage creates new element of the storage.
func NewPage(displayName string, template func(next, prev *string) templ.Component) *Page {
	return &Page{
		normalization.Normalize(displayName),
		displayName,
		template,
	}
}

// Book is linked list which stores templates of the book.
type Book = list.List[*Page]

// New returns new instance of TemplateStorage.
func New() *Book {
	b := list.New[*Page]()

	b.PushBack(NewPage("Úvod", intro))
	b.PushBack(NewPage("Lua", lua))
	b.PushBack(NewPage("Klávesové Zkratky", keymaps))
	b.PushBack(NewPage("Lazy", lazy))
	b.PushBack(NewPage("Lsp", lsp))
	b.PushBack(NewPage("Treesitter", treesitter))
	b.PushBack(NewPage("Komentování", comments))
	b.PushBack(NewPage("Git", git))
	b.PushBack(NewPage("Instalace", installation))

	return b
}

// Package ptr provides helper functions for dealing with pointers.
package ptr

// To coverts anything to a pointer.
func To[T any](t T) *T {
	return &t
}

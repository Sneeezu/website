// The main package
package main

import (
	"flag"

	"codeberg.org/Sneeezu/website/internal/handler"
)

var (
	addressFlag string
)

func init() {
	flag.StringVar(&addressFlag, "a", "localhost:3000", "Set address where to host the site.")
	flag.Parse()
}

func main() {
	handler.New().Serve(addressFlag)
}
